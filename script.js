let trainer = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachi", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock", "Misty"]
	},

	talk: function() {
		console.log("Pikachu! I choose you!")

	}

}

console.log(trainer)

console.log("Result of dot notation:")
console.log(trainer.name)

console.log("Result of square bracket notation:")
console.log(trainer.pokemon)


console.log("Result of talk() method:")
trainer.talk();

function pokemon(name,level) {
	this.name = name;
	this.level = level;
	this.health = level * 3;
	this.attack = level * 1.5;

	this.tackle = function(pokemon) {
		console.log(this.name + " tackled " +pokemon.name);
	}

	this.faint = function() {
		console.log(this.name + " fainted!");
	}

}


let pokemon1 = new pokemon("Pikachu", 12)
console.log(pokemon1);
let pokemon2 = new pokemon("Geodude", 8)
console.log(pokemon2);
let pokemon3 = new pokemon("Mewtwo", 100)
console.log(pokemon3);

pokemon2.tackle(pokemon1);
pokemon3.tackle(pokemon2);
pokemon3.faint();





